import * as React from 'react';
import './styles.scss';
export interface IButtonExampleProps {
    disabled?: boolean;
    checked?: boolean;
}
export declare const ButtonDefaultExample: React.FunctionComponent<IButtonExampleProps>;
export declare type ICounterProps = {
    className?: string;
};
export declare const App: React.FC<ICounterProps>;

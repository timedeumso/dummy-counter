"use strict";
exports.__esModule = true;
exports.App = exports.ButtonDefaultExample = void 0;
var React = require("react");
require("./styles.scss");
var office_ui_fabric_react_1 = require("office-ui-fabric-react");
// Example formatting
var stackTokens = { childrenGap: 40 };
exports.ButtonDefaultExample = function (props) {
    var disabled = props.disabled, checked = props.checked;
    return (<office_ui_fabric_react_1.Stack horizontal tokens={stackTokens}>
      <office_ui_fabric_react_1.DefaultButton text="Standard" onClick={_alertClicked} allowDisabledFocus disabled={disabled} checked={checked}/>
      <office_ui_fabric_react_1.PrimaryButton text="Primary" onClick={_alertClicked} allowDisabledFocus disabled={disabled} checked={checked}/>
    </office_ui_fabric_react_1.Stack>);
};
function _alertClicked() {
    alert('Clicked');
}
var useState = React.useState, useEffect = React.useEffect;
var Counter = function (_a) {
    var count = _a.count, className = _a.className;
    return (<div className={"counter " + className}>
    <p key={count} className={"counter__count " + (className ? className + '__count' : '')}>
      {count}
    </p>
  </div>);
};
exports.App = function (_a) {
    var _b = _a.className, className = _b === void 0 ? '' : _b;
    var _c = useState(0), count = _c[0], setCount = _c[1];
    useEffect(function () {
        var interval = setInterval(function () {
            if (count > 99)
                return setCount(0);
            setCount(count + 1);
        }, 1000);
        return function () { return clearInterval(interval); };
    }, [count, setCount]);
    return <Counter className={className} count={count}/>;
};
//# sourceMappingURL=index.js.map